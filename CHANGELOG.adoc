= Antora Assembler Changelog
:url-repo: https://gitlab.com/antora/antora-assembler

This document provides a summary of all notable changes to Antora Assembler by release.
For a detailed view of what's changed, refer to the {url-repo}/commits[commit history] of this project.

== 1.0.0-alpha.6 (2022-09-23)

=== Changed

* log warning message if include directive to reduce cannot be found

=== Fixed

* reify leveloffset to handle cases when leveloffset is set outside of document header
* properly reduce include directives that use the `leveloffset` attribute (#9)

== 1.0.0-alpha.5 (2022-07-24)

=== Fixed

* change default value for `component_versions` key to `*` as documented
* process document title and section titles inside AsciiDoc table cell correctly
* only use source-highlighter specified in Assembler config when converting assembled document
* always ensure that doctype attribute is set in assembler AsciiDoc config
* only set doctype attribute on aggregate document

== 1.0.0-alpha.4 (2022-07-18)

=== Fixed

* show command failed message if EPIPE is thrown (#8)
* fix rewriting of internal xref macro targets
* retain xref macro for internal ref if text part contains attributes

== 1.0.0-alpha.3 (2022-07-08)

=== Fixed

* process block image macro on first line of AsciiDoc table cell
* process multiple image macros on the same line inside a table

== 1.0.0-alpha.2 (2022-07-02)

=== Fixed

* Enable sourcemap even when Asciidoctor extensions are registered
* Unset source-highlighter attribute when producing aggregate document to avoid warnings

== 1.0.0-alpha.1 (2022-05-25)

_Initial prerelease of the software._

////
=== Added
=== Changed
=== Fixed
=== Removed
////
