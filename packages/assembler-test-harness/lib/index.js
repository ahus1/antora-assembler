/* eslint-env mocha */
'use strict'

const { configureLogger } = require('@antora/logger')
const { expect, use } = require('chai')
const createFile = require('./create-file')
const heredoc = require('./heredoc')
const loadScenario = require('./load-scenario')
const runScenario = require('./run-scenario')

use(require('dirty-chai'))

beforeEach(() => configureLogger({ level: 'all' })) // eslint-disable-line no-undef

module.exports = { createFile, heredoc, expect, loadScenario, runScenario }
