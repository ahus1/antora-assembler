/* eslint-env mocha */
'use strict'

const { createFile, expect } = require('@antora/assembler-test-harness')
const selectMutableAttributes = require('@antora/assembler/select-mutable-attributes')
const loadAsciiDoc = require('@antora/asciidoc-loader')

describe('selectMutableAttributes()', () => {
  let referencePage

  const buildReferencePage = () =>
    createFile({
      contents: Buffer.from('= Start Page'),
      component: 'the-component',
      version: '1.0',
      module: 'ROOT',
      family: 'page',
      relative: 'index.adoc',
      origin: {
        url: 'https://github.com/acme/the-component-docs',
        startPath: '',
        branch: 'v1.0',
        refhash: 'a00000000000000000000000000000000000000z',
      },
    })

  beforeEach(() => {
    referencePage = buildReferencePage()
  })

  it('should return default attributes that are defined and mutable', () => {
    const actual = selectMutableAttributes(loadAsciiDoc, undefined, referencePage)
    expect(actual.constructor).to.equal(Object)
    // TODO expand this list to stuff we think should be mutable
    expect(actual).to.include({
      'attribute-undefined': 'drop-line',
      'attribute-missing': 'skip',
      doctype: 'article',
      sectids: '',
      'appendix-caption': 'Appendix',
      'appendix-refsig': 'Appendix',
    })
    expect(actual).to.not.have.property('imagesdir')
    expect(actual).to.not.have.property('relfilesuffix')
  })

  it('should not include attributes hard set in AsciiDoc config', () => {
    const asciidocConfig = {
      attributes: {
        'attribute-missing': 'warn',
        sectids: '',
      },
    }
    const actual = selectMutableAttributes(loadAsciiDoc, undefined, referencePage, asciidocConfig)
    expect(actual.constructor).to.equal(Object)
    expect(actual).to.not.have.property('attribute-missing')
    expect(actual).to.not.have.property('sectids')
  })

  it('should not include attributes soft set in AsciiDoc config', () => {
    const asciidocConfig = {
      attributes: {
        'attribute-missing@': 'warn',
        'doctype@': 'book',
      },
    }
    const actual = selectMutableAttributes(loadAsciiDoc, undefined, referencePage, asciidocConfig)
    expect(actual.constructor).to.equal(Object)
    expect(actual).to.include({ 'attribute-missing': 'warn', doctype: 'book' })
  })

  it('should use empty clone of reference page', () => {
    referencePage.contents = Buffer.from('= Page Title\n:foo: bar')
    const actual = selectMutableAttributes(loadAsciiDoc, undefined, referencePage)
    expect(actual.constructor).to.equal(Object)
    expect(actual).to.not.have.property('foo')
  })

  it('should include implicit page attributes for component version', () => {
    const contentCatalog = {
      getComponent (name) {
        return { name, title: 'The Component', versions: [{ name, version: '1.0', displayVersion: '1.0' }] }
      },
    }
    const actual = selectMutableAttributes(loadAsciiDoc, contentCatalog, referencePage)
    expect(actual.constructor).to.equal(Object)
    // Q: what about page-module and page-relative-src-path?
    expect(actual).to.include({
      'page-component-name': 'the-component',
      'page-component-version': '1.0',
      'page-component-display-version': '1.0',
      'page-version': '1.0',
      'page-component-title': 'The Component',
    })
    // FIXME shouldn't the origin attributes be mutable?
    expect(actual).to.not.have.property('page-origin-url')
    expect(actual).to.not.have.property('relfilesuffix')
  })
})
