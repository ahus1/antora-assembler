'use strict'
;(async () => {
  const { stdin, stdout } = process
  for await (const chunk of stdin) stdout.write(chunk)
})()
