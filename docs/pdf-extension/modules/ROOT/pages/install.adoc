= Install the PDF Extension
:navtitle: Install the Extension

The Antora PDF extension requires Ruby 2.7 or greater, Asciidoctor PDF 2, Node.js 16 or greater, and Antora 3.

== Prerequisites

The instructions in the following sections assume you've already set up a playbook project, an Antora playbook file (i.e., [.path]_antora-playbook.yml_), and that you're executing the commands from your playbook project.

=== Antora playbook project

We highly recommend installing this software and its prerequisites in your xref:antora:playbook:use-an-existing-playbook-project.adoc[playbook project], that is, the directory where your site's Antora playbook file (e.g., [.path]_antora-playbook.yml_) is located.
If you install the prerequisites and PDF extension globally, conflicts with other installed software and versions may occur.

When you're evaluating Antora Assembler and the PDF extension for the first time, we recommend starting with the Antora demo.

 $ git clone https://gitlab.com/antora/demo/docs-site.git assembler-tutorial && cd assembler-tutorial

By starting out this way, you can learn how Antora Assembler without having to worry about other complexities.
Once you get that working, you can move on to using it on your own Antora playbook project while still being able to refer back to the demo.

=== Ruby

Asciidoctor PDF 2 requires either Ruby 2.7 or greater or JRuby 9.2 or greater.
We strongly encourage you to use the latest stable Ruby version.
To check if you have Ruby available, run the `ruby` command to print the installed version:

 $ ruby -v

You should see a version string, such as:

 $ ruby -v
 ruby 3.1.0p0 (2021-12-25 revision fb4df44d16) [x86_64-linux]

Make sure this command reports a Ruby version that starts with 2.7 or greater (or a JRuby version that starts with 9.2 or greater).
If it doesn't, go to {url-rvm}[rvm.io^] to set up RVM and use it to install a version of Ruby.

[#install-asciidoctor-pdf]
=== Asciidoctor PDF

The Antora PDF extension uses {url-pdf-docs}/[Asciidoctor PDF 2^] to generate PDF files.
We recommend {url-pdf-docs}/install/#install-asciidoctor-pdf[installing Asciidoctor PDF^] into your project using a [.path]_Gemfile_ managed by Bundler.

. Create a file named [.path]_Gemfile_ and add an entry for `asciidoctor-pdf` as follows:
+
.Gemfile
[,ruby]
----
source 'https://rubygems.org'

gem 'asciidoctor-pdf'
----

. Configure Bundler to install gems inside the project:

 $ bundle config --local path .bundle/gems

. Run Bundler as follows:

 $ bundle

. If you want to apply syntax highlighting to source blocks in your PDF files, you'll also need a syntax highlighter that is supported by Asciidoctor PDF.
Let's install Rouge by adding an entry for the `rouge` gem in the [.path]_Gemfile_.
+
.Gemfile
[,ruby]
----
source 'https://rubygems.org'

gem 'asciidoctor-pdf'
gem 'rouge'
----

. Run Bundler again.

 $ bundle

If you have problems installing Asciidoctor PDF, see the installation {url-pdf-docs}/install/#troubleshooting[troubleshooting tips^].

If you prefer to install Asciidoctor PDF globally, you can use the `gem install` command instead.
Pass the name of the gem, `asciidoctor-pdf`, to the `gem install` command as follows:

 $ gem install asciidoctor-pdf

NOTE: How you install Asciidoctor PDF will affect the value you assign to the xref:configure.adoc#build-command[build.command key].

=== Node.js

This software requires Node.js 16 or greater.
To see if you have Node.js installed, and which version, type:

 $ node -v

You should see a version string, such as:

[subs=attributes+]
 $ node -v
 v{node-full}

If no version number is displayed in your terminal, you need to install Node.js.
Follow one of these guides to learn how to install nvm and Node.js on your platform.

* xref:antora:install:linux-requirements.adoc#install-nvm[Install nvm and Node.js on Linux]
* xref:antora:install:macos-requirements.adoc#install-nvm[Install nvm and Node.js on macOS]
* xref:antora:install:windows-requirements.adoc#install-choco[Install nvm and Node.js on Windows]

=== Antora

Assembler and the PDF extension require Antora 3.
To confirm Antora 3 is installed in your playbook directory, run:

 $ npx antora -v

The command should report the version of the Antora CLI and site generator you have installed.

[subs=+attributes]
 $ npx antora -v
 @antora/cli: {core-minor}{core-patch}
 @antora/site-generator: {core-minor}{core-patch}

If you're not yet using Antora 3, xref:antora:install:upgrade-antora.adoc[upgrade to the latest version] to use the PDF extension.

Once you've satisfied the prerequisites, you're ready to install the Antora PDF extension in your playbook project.

[#install]
== Install the PDF extension package

To install the Antora PDF extension package in your playbook project, type the following command:

 $ npm i @antora/pdf-extension

This command installs the PDF extension and its dependencies, including the supporting Assembler library.
However, Antora won't automatically use the PDF extension unless it's registered.
You can xref:register.adoc[register the extension] in your playbook file or with CLI.